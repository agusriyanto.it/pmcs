<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mg_products', function (Blueprint $table) {
            $table->id();
            $table->integer('caty_id');
            $table->string('prod_name');
            $table->string('prod_buy');
            $table->string('prod_sell');
            $table->integer('prod_qty');
            $table->string('prod_unit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mg_products');
    }
};
