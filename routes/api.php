<?php

use App\Http\Controllers\API\InventoryCaty;
use App\Http\Controllers\API\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('inv-caty', [InventoryCaty::class, 'get']);
Route::get('inv-caty/{id}', [InventoryCaty::class, 'get']);
Route::post('inv-caty', [InventoryCaty::class, 'store']);
Route::put('inv-caty/{id}', [InventoryCaty::class, 'update']);
Route::delete('inv-caty/{id}', [InventoryCaty::class, 'delete']);

Route::get('product', [ProductController::class, 'index']);
Route::get('product/{id}', [ProductController::class, 'index']);
Route::post('product', [ProductController::class, 'store']);
Route::put('product/{id}', [ProductController::class, 'update']);
Route::delete('product/{id}', [ProductController::class, 'delete']);