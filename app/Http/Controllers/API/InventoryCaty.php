<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Md_inv_caty;
use Illuminate\Support\Facades\Validator;

class InventoryCaty extends Controller
{
    function get($id = null){
        if(isset($id)){
            $data['a0'] = Md_inv_caty::findOrFail($id);
            $data['a1'] = Md_inv_caty::whereNull('inv_host')->get();
            return response()->json(['msg' => 'Data retrieved', 'data' => $data], 200);
        }else{
            $data['a0'] = Md_inv_caty::get();
            $data['a1'] = Md_inv_caty::whereNull('inv_host')->get();
            return response()->json(['msg' => 'Data retrieved', 'data' => $data], 200);
        }
    }

    function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name'     => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create post
        $caty = Md_inv_caty::create([
            'inv_name'     => $request->name,
            'inv_sts'      => 1
        ]);

        //return response
        return response()->json(['msg' => 'Data created', 'data' => $caty], 201);
    }

    function update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name'     => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $caty = Md_inv_caty::findOrFail($id);
        $caty->update([
            'inv_name'     => $request->name,
            'inv_host'     => $request->host
        ]);
        return response()->json(['msg' => 'Data updated', 'data' => $caty], 200);
    }

    function delete($id){
        $caty = Md_inv_caty::findOrFail($id);
        $caty->delete();
        return response()->json(['msg' => 'Data deleted'], 200);
    }
}
