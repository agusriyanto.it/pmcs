<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Mg_product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        if(isset($id)){
            $data['a0'] = DB::table('mg_products as p')
                        ->join('md_inv_caties as i', 'i.id', '=', 'p.caty_id')
                        ->where('p.prod_id','=', $id)
                        ->get();
            return response()->json(['msg' => 'Data retrieved', 'data' => $data], 200);
        }else{
            $data['a0'] = DB::table('mg_products as p')
                        ->join('md_inv_caties as i', 'i.id', '=', 'p.caty_id')
                        ->get();
            return response()->json(['msg' => 'Data retrieved', 'data' => $data], 200);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'host'     => 'required',
            'name'     => 'required',
            'buy'     => 'required',
            'sell'     => 'required',
            'qty'     => 'required',
            'unit'     => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        //create post
        $data = Mg_product::create([
            'caty_id'     => $request->host,
            'prod_name'     => $request->name,
            'prod_buy'      => $request->buy,
            'prod_sell'      => $request->sell,
            'prod_unit'      => $request->unit,
            'prod_qty'      => $request->qty,
        ]);

        //return response
        return response()->json(['msg' => 'Data created', 'data' => $data], 201);
    }
    
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'host'     => 'required',
            'name'     => 'required',
            'buy'     => 'required',
            'sell'     => 'required',
            'qty'     => 'required',
            'unit'     => 'required'
        ]);

        //check if validation fails
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $caty = DB::table('mg_products as p')
                ->where('p.prod_id','=', $id);
        $caty->update([
            'caty_id'     => $request->host,
            'prod_name'     => $request->name,
            'prod_buy'      => $request->buy,
            'prod_sell'      => $request->sell,
            'prod_unit'      => $request->unit,
            'prod_qty'      => $request->qty,
        ]);
        return response()->json(['msg' => 'Data updated', 'data' => $caty], 200);
    }

    public function delete($id)
    {
        $caty = DB::table('mg_products as p')
                ->where('p.prod_id','=', $id);
        $caty->delete();
        return response()->json(['msg' => 'Data deleted'], 200);
    }
}
