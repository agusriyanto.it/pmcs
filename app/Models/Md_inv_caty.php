<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Md_inv_caty extends Model
{
    use HasFactory;

    protected $fillable = ['id','inv_host', 'inv_name', 'inv_sts'];

    public function product() {
        return $this->hasMany(Mg_product::class);
    }
}
