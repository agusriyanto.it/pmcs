<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mg_product extends Model
{
    use HasFactory;

    protected $fillable = ['caty_id','prod_name', 'prod_buy', 'prod_sell', 'prod_qty','prod_unit'];

    public function caty() {
        return $this->belongsTo(Md_inv_caty::class);
    }
}
